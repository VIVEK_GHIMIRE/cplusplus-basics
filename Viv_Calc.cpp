/* This file explains the common data types in C++ which are :

int --> Stores whole numbers without decimals ex: 3,44 etc ----> 2-4 bytes
char --> Stores single character/letter/number or any ASCII values --> 1 bytes
float -> stores numbers with decimals ---> 4 bytes
double-> Stores numbers like float but with more precision --> 8 bytes
string -> stores characters --> #include <string> */

//type all the datatypes one by one and print them 

#include <string>
#include <iostream>
using namespace std;

int a = 9;
int b = 10;



int main (){
	cout << "sum: " << a+ b << endl;
	cout << "Product: " <<a*b << "\n";
	cout << "Difference: " <<a-b << "\n";
return 0;
}
