//Pointers in C++ ----> created using '*' followed by a variable name
#include<iostream>
#include<string>
using namespace std;

int main(){
    string food = "Pizza"; // Created a variable by the name food and stored value 'pizza' in it
    string* ptr = &food; // declaring a pointer to the variable food


    // Reference : The below line will output the memory of the variable food
    cout << ptr << "\n";

    // Dereference : output the value of food variable with the pointer ptr
    cout << *ptr << "\n";



}
