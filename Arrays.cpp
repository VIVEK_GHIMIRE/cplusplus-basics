// ARRAYS can store multiple values
// Syntax --> Datatype Array_name[no. of items]; to declare empty array
// Syntax --> Datatype Array_name[no. of items] = {val1, val2, val3 ..., valN};

#include <iostream>
#include <string>
using namespace std;

int main (){


	string brands[4] = {"Puma", "Reebok", "Nike", "Addidas"};
	int myNum[3] = {1,2,3};

// In C++ indexing starts from 0 (Imagine this like a position number arranged in order)
// string brands[4] {"Puma", "Reebok", "Nike", "Addidas"}
// Indexing ------> 0, 1, 2, 3, 

	cout << brands[2] << "\n";
	
	return 0;

}

