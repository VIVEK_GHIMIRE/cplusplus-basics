// Structures ( struct ) are used to group several related variables
// Every object that uses the structure is called member of it
// Unlike array the structure can have different data types

//struct{     		// Initialization
//	int mynumber;   // Member integer called by mynumber
//	string mystring;// Member string called by mystring
//} myStructure;      // Variable called by myStructure that can access all the members of this structure

// To access the members of the structure the syntax will be "." ex: myStructure.mynumber....

#include <iostream>
#include <string>
using namespace std;

struct Car{
	string brand;
	string model;
	int year;
};

int main(){
	// Creating a car structure and storing it in as variable car1;
	Car car1;
	car1.brand = "Maruti";
	car1.model = "Swift";
	car1.year = 2020;

	Car car2;
	car2.brand = "Hyundai";
	car2.model = "i20";
	car2.year = 2021;

	// Print the structure members for our variables car1 and car2
	cout << car1.brand << "	" << car1.model<< "	" << car1.year<< "\n";
	cout << car2.brand << "	" << car2.model<< "	" << car2.year<< "\n";

	return 0;
}


