#include <iostream>
#include <string>
using namespace std;

int main (){			//Initialize my main function
	struct {			//Initialize my structure
		string brand;
		string model;
		int year;
	} iphone, note;	// create 2 variables (members)
	
	//Append value to members
	iphone.brand = "Apple";
	iphone.model = "13";
	iphone.year = 2022;	
	
	
	note.brand = "Samsung";
	note.model = "20";
	note.year = 2021;	
	
	cout << iphone.brand<<"\n";
	cout << note.model;
	
	return 0;

}
